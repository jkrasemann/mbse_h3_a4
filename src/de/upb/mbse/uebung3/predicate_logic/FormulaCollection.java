/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula Collection</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getFormulas <em>Formulas</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getTerms <em>Terms</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getFormulaCollection()
 * @model
 * @generated
 */
public interface FormulaCollection extends EObject {
	/**
	 * Returns the value of the '<em><b>Formulas</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.mbse.uebung3.predicate_logic.Formula}.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.Formula#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Formulas</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Formulas</em>' containment reference list.
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getFormulaCollection_Formulas()
	 * @see de.upb.mbse.uebung3.predicate_logic.Formula#getCollection
	 * @model opposite="collection" containment="true"
	 * @generated
	 */
	EList<Formula> getFormulas();

	/**
	 * Returns the value of the '<em><b>Terms</b></em>' containment reference list.
	 * The list contents are of type {@link de.upb.mbse.uebung3.predicate_logic.Term}.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.Term#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Terms</em>' containment reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Terms</em>' containment reference list.
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getFormulaCollection_Terms()
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getCollection
	 * @model opposite="collection" containment="true"
	 * @generated
	 */
	EList<Term> getTerms();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String toString();

} // FormulaCollection
