/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Formula;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.UnaryOperator;
import de.upb.mbse.uebung3.predicate_logic.Visitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Unary Operator</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryOperatorImpl#getSubFormula
 * <em>Sub Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class UnaryOperatorImpl extends FormulaImpl implements
		UnaryOperator {
	/**
	 * The cached value of the '{@link #getSubFormula() <em>Sub Formula</em>}'
	 * containment reference. <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getSubFormula()
	 * @generated
	 * @ordered
	 */
	protected Formula subFormula;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected UnaryOperatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.UNARY_OPERATOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Formula getSubFormula() {
		return subFormula;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSubFormula(Formula newSubFormula,
			NotificationChain msgs) {
		Formula oldSubFormula = subFormula;
		subFormula = newSubFormula;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA,
					oldSubFormula, newSubFormula);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSubFormula(Formula newSubFormula) {
		if (newSubFormula != subFormula) {
			NotificationChain msgs = null;
			if (subFormula != null)
				msgs = ((InternalEObject) subFormula)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA,
								null, msgs);
			if (newSubFormula != null)
				msgs = ((InternalEObject) newSubFormula)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA,
								null, msgs);
			msgs = basicSetSubFormula(newSubFormula, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA,
					newSubFormula, newSubFormula));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public String getOperatorSymbol() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA:
			return basicSetSubFormula(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA:
			return getSubFormula();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA:
			setSubFormula((Formula) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA:
			setSubFormula((Formula) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.UNARY_OPERATOR__SUB_FORMULA:
			return subFormula != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @return this operators subformula and its symbol in infix notation.
	 */
	public String toString() {

		StringBuffer result = new StringBuffer();
		result.append(getOperatorSymbol());
		result.append('(');
		result.append(subFormula.toString());
		result.append(')');

		return result.toString();
	}

} // UnaryOperatorImpl
