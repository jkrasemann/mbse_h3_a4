/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Implication;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Implication</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ImplicationImpl extends BinaryOperatorImpl implements Implication {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected ImplicationImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.IMPLICATION;
	}
	/**
	 * @return this classes String-representation, which is always '->' in this case.
	 */
	@Override
	public String getOperatorSymbol() {
		return " -> ";
	}
} //ImplicationImpl
