/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.BinaryOperator;
import de.upb.mbse.uebung3.predicate_logic.Formula;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Visitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Binary Operator</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl#getFirstSubFormula
 * <em>First Sub Formula</em>}</li>
 * <li>
 * {@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl#getSecondSubFormula
 * <em>Second Sub Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class BinaryOperatorImpl extends FormulaImpl implements
		BinaryOperator {
	/**
	 * The cached value of the '{@link #getFirstSubFormula()
	 * <em>First Sub Formula</em>}' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getFirstSubFormula()
	 * @generated
	 * @ordered
	 */
	protected Formula firstSubFormula;

	/**
	 * The cached value of the '{@link #getSecondSubFormula()
	 * <em>Second Sub Formula</em>}' containment reference. <!-- begin-user-doc
	 * --> <!-- end-user-doc -->
	 * 
	 * @see #getSecondSubFormula()
	 * @generated
	 * @ordered
	 */
	protected Formula secondSubFormula;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected BinaryOperatorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.BINARY_OPERATOR;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Formula getFirstSubFormula() {
		return firstSubFormula;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetFirstSubFormula(
			Formula newFirstSubFormula, NotificationChain msgs) {
		Formula oldFirstSubFormula = firstSubFormula;
		firstSubFormula = newFirstSubFormula;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA,
					oldFirstSubFormula, newFirstSubFormula);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setFirstSubFormula(Formula newFirstSubFormula) {
		if (newFirstSubFormula != firstSubFormula) {
			NotificationChain msgs = null;
			if (firstSubFormula != null)
				msgs = ((InternalEObject) firstSubFormula)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA,
								null, msgs);
			if (newFirstSubFormula != null)
				msgs = ((InternalEObject) newFirstSubFormula)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA,
								null, msgs);
			msgs = basicSetFirstSubFormula(newFirstSubFormula, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA,
					newFirstSubFormula, newFirstSubFormula));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Formula getSecondSubFormula() {
		return secondSubFormula;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetSecondSubFormula(
			Formula newSecondSubFormula, NotificationChain msgs) {
		Formula oldSecondSubFormula = secondSubFormula;
		secondSubFormula = newSecondSubFormula;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this,
					Notification.SET,
					Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA,
					oldSecondSubFormula, newSecondSubFormula);
			if (msgs == null)
				msgs = notification;
			else
				msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setSecondSubFormula(Formula newSecondSubFormula) {
		if (newSecondSubFormula != secondSubFormula) {
			NotificationChain msgs = null;
			if (secondSubFormula != null)
				msgs = ((InternalEObject) secondSubFormula)
						.eInverseRemove(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA,
								null, msgs);
			if (newSecondSubFormula != null)
				msgs = ((InternalEObject) newSecondSubFormula)
						.eInverseAdd(
								this,
								EOPPOSITE_FEATURE_BASE
										- Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA,
								null, msgs);
			msgs = basicSetSecondSubFormula(newSecondSubFormula, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA,
					newSecondSubFormula, newSecondSubFormula));
	}

	/**
	 * @return the symbol of this operator (e.g. 'AND', 'OR', ...)
	 */
	public abstract String getOperatorSymbol();

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA:
			return basicSetFirstSubFormula(null, msgs);
		case Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA:
			return basicSetSecondSubFormula(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA:
			return getFirstSubFormula();
		case Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA:
			return getSecondSubFormula();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA:
			setFirstSubFormula((Formula) newValue);
			return;
		case Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA:
			setSecondSubFormula((Formula) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA:
			setFirstSubFormula((Formula) null);
			return;
		case Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA:
			setSecondSubFormula((Formula) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.BINARY_OPERATOR__FIRST_SUB_FORMULA:
			return firstSubFormula != null;
		case Predicate_logicPackage.BINARY_OPERATOR__SECOND_SUB_FORMULA:
			return secondSubFormula != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @return this operators subformulas and its symbol in infix notation.
	 */
	public String toString() {

		StringBuffer result = new StringBuffer();
		result.append(firstSubFormula.toString());
		result.append(getOperatorSymbol());
		result.append(secondSubFormula.toString());

		return result.toString();
	}

	/**
	 * Let the Visitor v visit this class.
	 */
	public void accept(Visitor v) {
		v.visit(this);
	}

} // BinaryOperatorImpl
