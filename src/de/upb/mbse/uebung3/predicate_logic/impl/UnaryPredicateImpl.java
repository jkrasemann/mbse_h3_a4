/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Term;
import de.upb.mbse.uebung3.predicate_logic.UnaryPredicate;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Unary Predicate</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryPredicateImpl#getFirstTermUnary <em>First Term Unary</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class UnaryPredicateImpl extends AtomicFormulaImpl implements
		UnaryPredicate {
	/**
	 * The cached value of the '{@link #getFirstTermUnary() <em>First Term Unary</em>}' reference.
	 * <!-- begin-user-doc --> <!--
	 * end-user-doc -->
	 * @see #getFirstTermUnary()
	 * @generated
	 * @ordered
	 */
	protected Term firstTermUnary;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected UnaryPredicateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.UNARY_PREDICATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term getFirstTermUnary() {
		if (firstTermUnary != null && firstTermUnary.eIsProxy()) {
			InternalEObject oldFirstTermUnary = (InternalEObject)firstTermUnary;
			firstTermUnary = (Term)eResolveProxy(oldFirstTermUnary);
			if (firstTermUnary != oldFirstTermUnary) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY, oldFirstTermUnary, firstTermUnary));
			}
		}
		return firstTermUnary;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term basicGetFirstTermUnary() {
		return firstTermUnary;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstTermUnary(Term newFirstTermUnary,
			NotificationChain msgs) {
		Term oldFirstTermUnary = firstTermUnary;
		firstTermUnary = newFirstTermUnary;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY, oldFirstTermUnary, newFirstTermUnary);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstTermUnary(Term newFirstTermUnary) {
		if (newFirstTermUnary != firstTermUnary) {
			NotificationChain msgs = null;
			if (firstTermUnary != null)
				msgs = ((InternalEObject)firstTermUnary).eInverseRemove(this, Predicate_logicPackage.TERM__FIRST_TERM_UNARY, Term.class, msgs);
			if (newFirstTermUnary != null)
				msgs = ((InternalEObject)newFirstTermUnary).eInverseAdd(this, Predicate_logicPackage.TERM__FIRST_TERM_UNARY, Term.class, msgs);
			msgs = basicSetFirstTermUnary(newFirstTermUnary, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY, newFirstTermUnary, newFirstTermUnary));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				if (firstTermUnary != null)
					msgs = ((InternalEObject)firstTermUnary).eInverseRemove(this, Predicate_logicPackage.TERM__FIRST_TERM_UNARY, Term.class, msgs);
				return basicSetFirstTermUnary((Term)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				return basicSetFirstTermUnary(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				if (resolve) return getFirstTermUnary();
				return basicGetFirstTermUnary();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				setFirstTermUnary((Term)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				setFirstTermUnary((Term)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY:
				return firstTermUnary != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Returns a String that contains this predicates name and the term in this
	 * predicate in infix notation.
	 */
	public String toString() {

		StringBuffer result = new StringBuffer();

		result.append(name);
		result.append('(');
		result.append(getFirstTermUnary().toString());
		result.append(')');

		return result.toString();
	}

} // UnaryPredicateImpl
