/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Disjunction;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Disjunction</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class DisjunctionImpl extends BinaryOperatorImpl implements Disjunction {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected DisjunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.DISJUNCTION;
	}

	/**
	 * @return this classes String-representation, which is always 'OR' in this
	 *         case.
	 */
	@Override
	public String getOperatorSymbol() {
		return " OR ";
	}
} // DisjunctionImpl
