/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Formula;
import de.upb.mbse.uebung3.predicate_logic.FormulaCollection;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;

import de.upb.mbse.uebung3.predicate_logic.Visitor;
import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EcoreUtil;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Formula</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>
 * {@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaImpl#getCollection
 * <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class FormulaImpl extends EObjectImpl implements Formula {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected FormulaImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.FORMULA;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public FormulaCollection getCollection() {
		if (eContainerFeatureID() != Predicate_logicPackage.FORMULA__COLLECTION)
			return null;
		return (FormulaCollection) eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public NotificationChain basicSetCollection(
			FormulaCollection newCollection, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject) newCollection,
				Predicate_logicPackage.FORMULA__COLLECTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setCollection(FormulaCollection newCollection) {
		if (newCollection != eInternalContainer()
				|| (eContainerFeatureID() != Predicate_logicPackage.FORMULA__COLLECTION && newCollection != null)) {
			if (EcoreUtil.isAncestor(this, newCollection))
				throw new IllegalArgumentException(
						"Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCollection != null)
				msgs = ((InternalEObject) newCollection).eInverseAdd(this,
						Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS,
						FormulaCollection.class, msgs);
			msgs = basicSetCollection(newCollection, msgs);
			if (msgs != null)
				msgs.dispatch();
		} else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Predicate_logicPackage.FORMULA__COLLECTION, newCollection,
					newCollection));
	}

	/**
	 * <!-- begin-user-doc --> The FormulaImpl class has no relevant attributes
	 * to print out, so the implementation of this method is delegated to
	 * classes that extend FormulaImpl. <!-- end-user-doc -->
	 */
	public abstract String toString();

	/**
	 * Abstract accept-method (this is abstract because a Formula object has no
	 * relevant information to print to the user). Therefore, Visitor does not
	 * accept Formula objects, and implementation of this method has to be done
	 * in the subclasses.
	 */
	public abstract void accept(Visitor v);

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			return basicSetCollection((FormulaCollection) otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			return basicSetCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(
			NotificationChain msgs) {
		switch (eContainerFeatureID()) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			return eInternalContainer().eInverseRemove(this,
					Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS,
					FormulaCollection.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			return getCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			setCollection((FormulaCollection) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			setCollection((FormulaCollection) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.FORMULA__COLLECTION:
			return getCollection() != null;
		}
		return super.eIsSet(featureID);
	}

} // FormulaImpl
