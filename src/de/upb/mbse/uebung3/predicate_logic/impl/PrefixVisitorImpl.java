/**
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.AtomicFormula;
import de.upb.mbse.uebung3.predicate_logic.BinaryOperator;
import de.upb.mbse.uebung3.predicate_logic.BinaryPredicate;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.PrefixVisitor;
import de.upb.mbse.uebung3.predicate_logic.Quantifier;
import de.upb.mbse.uebung3.predicate_logic.Term;
import de.upb.mbse.uebung3.predicate_logic.UnaryOperator;
import de.upb.mbse.uebung3.predicate_logic.UnaryPredicate;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.impl.EObjectImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Prefix Visitor</b></em>'. Prints the given formula(s) in prefix
 * notation. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class PrefixVisitorImpl extends EObjectImpl implements PrefixVisitor {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected PrefixVisitorImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.PREFIX_VISITOR;
	}

	/**
	 * <!-- begin-user-doc --> Print this terms name. <!-- end-user-doc -->
	 */
	public void visit(Term t) {
		System.out.print(t.getName());
	}

	/**
	 * <!-- begin-user-doc --> Print the binary operators symbol, then visit its
	 * formulas. <!-- end-user-doc -->
	 */
	public void visit(BinaryOperator b) {

		System.out.print(b.getOperatorSymbol());
		b.getFirstSubFormula().accept(this);
		System.out.print(' ');
		b.getSecondSubFormula().accept(this);

	}

	/**
	 * <!-- begin-user-doc -->Print the unary operators symbol, then visits its
	 * formula.<!-- end-user-doc -->
	 */
	public void visit(UnaryOperator u) {

		System.out.print(u.getOperatorSymbol());
		u.getSubFormula().accept(this);
	}

	/**
	 * <!-- begin-user-doc -->Print the quantifiers symbol, then visit its
	 * formula. <!-- end-user-doc -->
	 */
	public void visit(Quantifier q) {

		System.out.print(q.getOperatorSymbol() + " " + q.getHas());
		System.out.print('(');
		q.getSubFormula().accept(this);
		System.out.print(')');
	}

	/**
	 * <!-- begin-user-doc --> Print the atomic formulas name. Then, visit its
	 * subformula(s).<!-- end-user-doc -->
	 */
	public void visit(AtomicFormula a) {

		System.out.print(a.getName());
		System.out.print('(');
		if (a instanceof UnaryPredicate) {
			UnaryPredicate u = (UnaryPredicate) a;
			u.getFirstTermUnary().accept(this);
		} else if (a instanceof BinaryPredicate) {
			BinaryPredicate b = (BinaryPredicate) a;
			b.getFirstTerm().accept(this);
			System.out.print(',');
			b.getSecondTerm().accept(this);
		}
		System.out.print(')');
	}

} // PrefixVisitorImpl
