/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.*;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Predicate_logicFactoryImpl extends EFactoryImpl implements Predicate_logicFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Predicate_logicFactory init() {
		try {
			Predicate_logicFactory thePredicate_logicFactory = (Predicate_logicFactory)EPackage.Registry.INSTANCE.getEFactory(Predicate_logicPackage.eNS_URI);
			if (thePredicate_logicFactory != null) {
				return thePredicate_logicFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Predicate_logicFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Predicate_logicFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Predicate_logicPackage.ATOMIC_FORMULA: return createAtomicFormula();
			case Predicate_logicPackage.UNARY_PREDICATE: return createUnaryPredicate();
			case Predicate_logicPackage.BINARY_PREDICATE: return createBinaryPredicate();
			case Predicate_logicPackage.CONSTANT_SYMBOL: return createConstantSymbol();
			case Predicate_logicPackage.VARIABLE: return createVariable();
			case Predicate_logicPackage.CONJUNCTION: return createConjunction();
			case Predicate_logicPackage.DISJUNCTION: return createDisjunction();
			case Predicate_logicPackage.IMPLICATION: return createImplication();
			case Predicate_logicPackage.EXISTENTIAL: return createExistential();
			case Predicate_logicPackage.UNIVERSAL: return createUniversal();
			case Predicate_logicPackage.NEGATION: return createNegation();
			case Predicate_logicPackage.FORMULA_COLLECTION: return createFormulaCollection();
			case Predicate_logicPackage.PREFIX_VISITOR: return createPrefixVisitor();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public AtomicFormula createAtomicFormula() {
		AtomicFormulaImpl atomicFormula = new AtomicFormulaImpl();
		return atomicFormula;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public UnaryPredicate createUnaryPredicate() {
		UnaryPredicateImpl unaryPredicate = new UnaryPredicateImpl();
		return unaryPredicate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public BinaryPredicate createBinaryPredicate() {
		BinaryPredicateImpl binaryPredicate = new BinaryPredicateImpl();
		return binaryPredicate;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public ConstantSymbol createConstantSymbol() {
		ConstantSymbolImpl constantSymbol = new ConstantSymbolImpl();
		return constantSymbol;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Variable createVariable() {
		VariableImpl variable = new VariableImpl();
		return variable;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Conjunction createConjunction() {
		ConjunctionImpl conjunction = new ConjunctionImpl();
		return conjunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Disjunction createDisjunction() {
		DisjunctionImpl disjunction = new DisjunctionImpl();
		return disjunction;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Implication createImplication() {
		ImplicationImpl implication = new ImplicationImpl();
		return implication;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Existential createExistential() {
		ExistentialImpl existential = new ExistentialImpl();
		return existential;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Universal createUniversal() {
		UniversalImpl universal = new UniversalImpl();
		return universal;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Negation createNegation() {
		NegationImpl negation = new NegationImpl();
		return negation;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaCollection createFormulaCollection() {
		FormulaCollectionImpl formulaCollection = new FormulaCollectionImpl();
		return formulaCollection;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public PrefixVisitor createPrefixVisitor() {
		PrefixVisitorImpl prefixVisitor = new PrefixVisitorImpl();
		return prefixVisitor;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Predicate_logicPackage getPredicate_logicPackage() {
		return (Predicate_logicPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Predicate_logicPackage getPackage() {
		return Predicate_logicPackage.eINSTANCE;
	}

} //Predicate_logicFactoryImpl
