/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Existential;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Existential</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ExistentialImpl extends QuantifierImpl implements Existential {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected ExistentialImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.EXISTENTIAL;
	}

	/**
	 * @return this classes String-representation, which is always 'exists' in
	 *         this case.
	 */
	@Override
	public String getOperatorSymbol() {
		return " EXISTS";
	}
} // ExistentialImpl
