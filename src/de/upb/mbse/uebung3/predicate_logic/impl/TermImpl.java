/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.BinaryPredicate;
import de.upb.mbse.uebung3.predicate_logic.FormulaCollection;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Term;
import de.upb.mbse.uebung3.predicate_logic.UnaryPredicate;
import de.upb.mbse.uebung3.predicate_logic.Visitor;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.EObjectImpl;
import org.eclipse.emf.ecore.util.EObjectWithInverseResolvingEList;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl#getSecondTerm <em>Second Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl#getFirstTerm <em>First Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl#getFirstTermUnary <em>First Term Unary</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl#getCollection <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class TermImpl extends EObjectImpl implements Term {
	/**
	 * The cached value of the '{@link #getSecondTerm() <em>Second Term</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getSecondTerm()
	 * @generated
	 * @ordered
	 */
	protected EList<BinaryPredicate> secondTerm;

	/**
	 * The cached value of the '{@link #getFirstTerm() <em>First Term</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstTerm()
	 * @generated
	 * @ordered
	 */
	protected EList<BinaryPredicate> firstTerm;

	/**
	 * The cached value of the '{@link #getFirstTermUnary() <em>First Term Unary</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFirstTermUnary()
	 * @generated
	 * @ordered
	 */
	protected EList<UnaryPredicate> firstTermUnary;

	/**
	 * The default value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected static final String NAME_EDEFAULT = null;

	/**
	 * The cached value of the '{@link #getName() <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getName()
	 * @generated
	 * @ordered
	 */
	protected String name = NAME_EDEFAULT;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected TermImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.TERM;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BinaryPredicate> getSecondTerm() {
		if (secondTerm == null) {
			secondTerm = new EObjectWithInverseResolvingEList<BinaryPredicate>(BinaryPredicate.class, this, Predicate_logicPackage.TERM__SECOND_TERM, Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM);
		}
		return secondTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<BinaryPredicate> getFirstTerm() {
		if (firstTerm == null) {
			firstTerm = new EObjectWithInverseResolvingEList<BinaryPredicate>(BinaryPredicate.class, this, Predicate_logicPackage.TERM__FIRST_TERM, Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM);
		}
		return firstTerm;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<UnaryPredicate> getFirstTermUnary() {
		if (firstTermUnary == null) {
			firstTermUnary = new EObjectWithInverseResolvingEList<UnaryPredicate>(UnaryPredicate.class, this, Predicate_logicPackage.TERM__FIRST_TERM_UNARY, Predicate_logicPackage.UNARY_PREDICATE__FIRST_TERM_UNARY);
		}
		return firstTermUnary;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String getName() {
		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setName(String newName) {
		String oldName = name;
		name = newName;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.TERM__NAME, oldName, name));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public FormulaCollection getCollection() {
		if (eContainerFeatureID() != Predicate_logicPackage.TERM__COLLECTION) return null;
		return (FormulaCollection)eInternalContainer();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetCollection(FormulaCollection newCollection, NotificationChain msgs) {
		msgs = eBasicSetContainer((InternalEObject)newCollection, Predicate_logicPackage.TERM__COLLECTION, msgs);
		return msgs;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setCollection(FormulaCollection newCollection) {
		if (newCollection != eInternalContainer() || (eContainerFeatureID() != Predicate_logicPackage.TERM__COLLECTION && newCollection != null)) {
			if (EcoreUtil.isAncestor(this, newCollection))
				throw new IllegalArgumentException("Recursive containment not allowed for " + toString());
			NotificationChain msgs = null;
			if (eInternalContainer() != null)
				msgs = eBasicRemoveFromContainer(msgs);
			if (newCollection != null)
				msgs = ((InternalEObject)newCollection).eInverseAdd(this, Predicate_logicPackage.FORMULA_COLLECTION__TERMS, FormulaCollection.class, msgs);
			msgs = basicSetCollection(newCollection, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.TERM__COLLECTION, newCollection, newCollection));
	}

	/**
	 * @return this terms name.
	 */
	public String toString() {

		return name;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getSecondTerm()).basicAdd(otherEnd, msgs);
			case Predicate_logicPackage.TERM__FIRST_TERM:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFirstTerm()).basicAdd(otherEnd, msgs);
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFirstTermUnary()).basicAdd(otherEnd, msgs);
			case Predicate_logicPackage.TERM__COLLECTION:
				if (eInternalContainer() != null)
					msgs = eBasicRemoveFromContainer(msgs);
				return basicSetCollection((FormulaCollection)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				return ((InternalEList<?>)getSecondTerm()).basicRemove(otherEnd, msgs);
			case Predicate_logicPackage.TERM__FIRST_TERM:
				return ((InternalEList<?>)getFirstTerm()).basicRemove(otherEnd, msgs);
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				return ((InternalEList<?>)getFirstTermUnary()).basicRemove(otherEnd, msgs);
			case Predicate_logicPackage.TERM__COLLECTION:
				return basicSetCollection(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eBasicRemoveFromContainerFeature(NotificationChain msgs) {
		switch (eContainerFeatureID()) {
			case Predicate_logicPackage.TERM__COLLECTION:
				return eInternalContainer().eInverseRemove(this, Predicate_logicPackage.FORMULA_COLLECTION__TERMS, FormulaCollection.class, msgs);
		}
		return super.eBasicRemoveFromContainerFeature(msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				return getSecondTerm();
			case Predicate_logicPackage.TERM__FIRST_TERM:
				return getFirstTerm();
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				return getFirstTermUnary();
			case Predicate_logicPackage.TERM__NAME:
				return getName();
			case Predicate_logicPackage.TERM__COLLECTION:
				return getCollection();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				getSecondTerm().clear();
				getSecondTerm().addAll((Collection<? extends BinaryPredicate>)newValue);
				return;
			case Predicate_logicPackage.TERM__FIRST_TERM:
				getFirstTerm().clear();
				getFirstTerm().addAll((Collection<? extends BinaryPredicate>)newValue);
				return;
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				getFirstTermUnary().clear();
				getFirstTermUnary().addAll((Collection<? extends UnaryPredicate>)newValue);
				return;
			case Predicate_logicPackage.TERM__NAME:
				setName((String)newValue);
				return;
			case Predicate_logicPackage.TERM__COLLECTION:
				setCollection((FormulaCollection)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				getSecondTerm().clear();
				return;
			case Predicate_logicPackage.TERM__FIRST_TERM:
				getFirstTerm().clear();
				return;
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				getFirstTermUnary().clear();
				return;
			case Predicate_logicPackage.TERM__NAME:
				setName(NAME_EDEFAULT);
				return;
			case Predicate_logicPackage.TERM__COLLECTION:
				setCollection((FormulaCollection)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.TERM__SECOND_TERM:
				return secondTerm != null && !secondTerm.isEmpty();
			case Predicate_logicPackage.TERM__FIRST_TERM:
				return firstTerm != null && !firstTerm.isEmpty();
			case Predicate_logicPackage.TERM__FIRST_TERM_UNARY:
				return firstTermUnary != null && !firstTermUnary.isEmpty();
			case Predicate_logicPackage.TERM__NAME:
				return NAME_EDEFAULT == null ? name != null : !NAME_EDEFAULT.equals(name);
			case Predicate_logicPackage.TERM__COLLECTION:
				return getCollection() != null;
		}
		return super.eIsSet(featureID);
	}
	
	public void accept(Visitor v) {
		v.visit(this);
	}

} //TermImpl
