/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.BinaryPredicate;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Term;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.common.notify.NotificationChain;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Binary Predicate</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl#getSecondTerm <em>Second Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl#getFirstTerm <em>First Term</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class BinaryPredicateImpl extends AtomicFormulaImpl implements
		BinaryPredicate {
	/**
	 * The cached value of the '{@link #getSecondTerm() <em>Second Term</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getSecondTerm()
	 * @generated
	 * @ordered
	 */
	protected Term secondTerm;

	/**
	 * The cached value of the '{@link #getFirstTerm() <em>First Term</em>}' reference.
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @see #getFirstTerm()
	 * @generated
	 * @ordered
	 */
	protected Term firstTerm;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	protected BinaryPredicateImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.BINARY_PREDICATE;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term getSecondTerm() {
		if (secondTerm != null && secondTerm.eIsProxy()) {
			InternalEObject oldSecondTerm = (InternalEObject)secondTerm;
			secondTerm = (Term)eResolveProxy(oldSecondTerm);
			if (secondTerm != oldSecondTerm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM, oldSecondTerm, secondTerm));
			}
		}
		return secondTerm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term basicGetSecondTerm() {
		return secondTerm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetSecondTerm(Term newSecondTerm,
			NotificationChain msgs) {
		Term oldSecondTerm = secondTerm;
		secondTerm = newSecondTerm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM, oldSecondTerm, newSecondTerm);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setSecondTerm(Term newSecondTerm) {
		if (newSecondTerm != secondTerm) {
			NotificationChain msgs = null;
			if (secondTerm != null)
				msgs = ((InternalEObject)secondTerm).eInverseRemove(this, Predicate_logicPackage.TERM__SECOND_TERM, Term.class, msgs);
			if (newSecondTerm != null)
				msgs = ((InternalEObject)newSecondTerm).eInverseAdd(this, Predicate_logicPackage.TERM__SECOND_TERM, Term.class, msgs);
			msgs = basicSetSecondTerm(newSecondTerm, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM, newSecondTerm, newSecondTerm));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term getFirstTerm() {
		if (firstTerm != null && firstTerm.eIsProxy()) {
			InternalEObject oldFirstTerm = (InternalEObject)firstTerm;
			firstTerm = (Term)eResolveProxy(oldFirstTerm);
			if (firstTerm != oldFirstTerm) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM, oldFirstTerm, firstTerm));
			}
		}
		return firstTerm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public Term basicGetFirstTerm() {
		return firstTerm;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public NotificationChain basicSetFirstTerm(Term newFirstTerm,
			NotificationChain msgs) {
		Term oldFirstTerm = firstTerm;
		firstTerm = newFirstTerm;
		if (eNotificationRequired()) {
			ENotificationImpl notification = new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM, oldFirstTerm, newFirstTerm);
			if (msgs == null) msgs = notification; else msgs.add(notification);
		}
		return msgs;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	public void setFirstTerm(Term newFirstTerm) {
		if (newFirstTerm != firstTerm) {
			NotificationChain msgs = null;
			if (firstTerm != null)
				msgs = ((InternalEObject)firstTerm).eInverseRemove(this, Predicate_logicPackage.TERM__FIRST_TERM, Term.class, msgs);
			if (newFirstTerm != null)
				msgs = ((InternalEObject)newFirstTerm).eInverseAdd(this, Predicate_logicPackage.TERM__FIRST_TERM, Term.class, msgs);
			msgs = basicSetFirstTerm(newFirstTerm, msgs);
			if (msgs != null) msgs.dispatch();
		}
		else if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM, newFirstTerm, newFirstTerm));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				if (secondTerm != null)
					msgs = ((InternalEObject)secondTerm).eInverseRemove(this, Predicate_logicPackage.TERM__SECOND_TERM, Term.class, msgs);
				return basicSetSecondTerm((Term)otherEnd, msgs);
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				if (firstTerm != null)
					msgs = ((InternalEObject)firstTerm).eInverseRemove(this, Predicate_logicPackage.TERM__FIRST_TERM, Term.class, msgs);
				return basicSetFirstTerm((Term)otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd,
			int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				return basicSetSecondTerm(null, msgs);
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				return basicSetFirstTerm(null, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				if (resolve) return getSecondTerm();
				return basicGetSecondTerm();
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				if (resolve) return getFirstTerm();
				return basicGetFirstTerm();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				setSecondTerm((Term)newValue);
				return;
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				setFirstTerm((Term)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				setSecondTerm((Term)null);
				return;
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				setFirstTerm((Term)null);
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.BINARY_PREDICATE__SECOND_TERM:
				return secondTerm != null;
			case Predicate_logicPackage.BINARY_PREDICATE__FIRST_TERM:
				return firstTerm != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * Returns a String that contains this predicates name and the terms in this
	 * predicate in infix notation.
	 */
	public String toString() {

		StringBuffer result = new StringBuffer();

		result.append(name);
		result.append('(');
		result.append(getFirstTerm().toString());
		result.append(',');
		result.append(getSecondTerm().toString());
		result.append(')');

		return result.toString();
	}
} // BinaryPredicateImpl
