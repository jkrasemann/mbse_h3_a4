/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Quantifier;
import de.upb.mbse.uebung3.predicate_logic.Variable;
import de.upb.mbse.uebung3.predicate_logic.Visitor;

import org.eclipse.emf.common.notify.Notification;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.emf.ecore.impl.ENotificationImpl;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Quantifier</b></em>'. <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 * <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.QuantifierImpl#getHas
 * <em>Has</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public abstract class QuantifierImpl extends UnaryOperatorImpl implements
		Quantifier {
	/**
	 * The cached value of the '{@link #getHas() <em>Has</em>}' reference. <!--
	 * begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @see #getHas()
	 * @generated
	 * @ordered
	 */
	protected Variable has;

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected QuantifierImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.QUANTIFIER;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Variable getHas() {
		if (has != null && has.eIsProxy()) {
			InternalEObject oldHas = (InternalEObject) has;
			has = (Variable) eResolveProxy(oldHas);
			if (has != oldHas) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE,
							Predicate_logicPackage.QUANTIFIER__HAS, oldHas, has));
			}
		}
		return has;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public Variable basicGetHas() {
		return has;
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	public void setHas(Variable newHas) {
		Variable oldHas = has;
		has = newHas;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET,
					Predicate_logicPackage.QUANTIFIER__HAS, oldHas, has));
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
		case Predicate_logicPackage.QUANTIFIER__HAS:
			if (resolve)
				return getHas();
			return basicGetHas();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
		case Predicate_logicPackage.QUANTIFIER__HAS:
			setHas((Variable) newValue);
			return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.QUANTIFIER__HAS:
			setHas((Variable) null);
			return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
		case Predicate_logicPackage.QUANTIFIER__HAS:
			return has != null;
		}
		return super.eIsSet(featureID);
	}

	/**
	 * @return this quantifiers subformula, its symbol and the bound variable in
	 *         infix notation.
	 */
	public String toString() {

		StringBuffer result = new StringBuffer();
		result.append(getOperatorSymbol());
		result.append(' ');
		result.append(has.toString());
		result.append('(');
		result.append(subFormula.toString());
		result.append(')');

		return result.toString();
	}

	/**
	 * Let the Visitor v visit this class.
	 */
	public void accept(Visitor v) {
		v.visit(this);
	}

} // QuantifierImpl
