/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Formula;
import de.upb.mbse.uebung3.predicate_logic.FormulaCollection;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Term;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.EObjectImpl;

import org.eclipse.emf.ecore.util.EObjectContainmentWithInverseEList;
import org.eclipse.emf.ecore.util.InternalEList;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Formula Collection</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl#getFormulas <em>Formulas</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl#getTerms <em>Terms</em>}</li>
 * </ul>
 * </p>
 *
 * @generated
 */
public class FormulaCollectionImpl extends EObjectImpl implements FormulaCollection {
	/**
	 * The cached value of the '{@link #getFormulas() <em>Formulas</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getFormulas()
	 * @generated
	 * @ordered
	 */
	protected EList<Formula> formulas;

	/**
	 * The cached value of the '{@link #getTerms() <em>Terms</em>}' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTerms()
	 * @generated
	 * @ordered
	 */
	protected EList<Term> terms;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected FormulaCollectionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.FORMULA_COLLECTION;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Formula> getFormulas() {
		if (formulas == null) {
			formulas = new EObjectContainmentWithInverseEList<Formula>(Formula.class, this, Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS, Predicate_logicPackage.FORMULA__COLLECTION);
		}
		return formulas;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Term> getTerms() {
		if (terms == null) {
			terms = new EObjectContainmentWithInverseEList<Term>(Term.class, this, Predicate_logicPackage.FORMULA_COLLECTION__TERMS, Predicate_logicPackage.TERM__COLLECTION);
		}
		return terms;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public String toString() {
		// TODO: implement this method
		// Ensure that you remove @generated or mark it @generated NOT
		throw new UnsupportedOperationException();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public NotificationChain eInverseAdd(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getFormulas()).basicAdd(otherEnd, msgs);
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				return ((InternalEList<InternalEObject>)(InternalEList<?>)getTerms()).basicAdd(otherEnd, msgs);
		}
		return super.eInverseAdd(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				return ((InternalEList<?>)getFormulas()).basicRemove(otherEnd, msgs);
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				return ((InternalEList<?>)getTerms()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				return getFormulas();
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				return getTerms();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				getFormulas().clear();
				getFormulas().addAll((Collection<? extends Formula>)newValue);
				return;
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				getTerms().clear();
				getTerms().addAll((Collection<? extends Term>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				getFormulas().clear();
				return;
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				getTerms().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Predicate_logicPackage.FORMULA_COLLECTION__FORMULAS:
				return formulas != null && !formulas.isEmpty();
			case Predicate_logicPackage.FORMULA_COLLECTION__TERMS:
				return terms != null && !terms.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //FormulaCollectionImpl
