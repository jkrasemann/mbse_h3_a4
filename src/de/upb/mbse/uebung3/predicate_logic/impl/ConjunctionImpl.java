/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic.impl;

import de.upb.mbse.uebung3.predicate_logic.Conjunction;
import de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage;
import de.upb.mbse.uebung3.predicate_logic.Visitor;

import org.eclipse.emf.ecore.EClass;

/**
 * <!-- begin-user-doc --> An implementation of the model object '
 * <em><b>Conjunction</b></em>'. <!-- end-user-doc -->
 * <p>
 * </p>
 *
 * @generated
 */
public class ConjunctionImpl extends BinaryOperatorImpl implements Conjunction {
	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	protected ConjunctionImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Predicate_logicPackage.Literals.CONJUNCTION;
	}

	/**
	 * @return this classes String-representation, which is always 'AND' in this
	 *         case.
	 */
	@Override
	public String getOperatorSymbol() {
		return " AND ";
	}

	/**
	 * Let the Visitor v visit this class.
	 */
	public void accept(Visitor v) {
		v.visit(this);
	}
} // ConjunctionImpl
