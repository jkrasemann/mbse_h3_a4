/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm <em>Second Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm <em>First Term</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryPredicate()
 * @model
 * @generated
 */
public interface BinaryPredicate extends AtomicFormula {
	/**
	 * Returns the value of the '<em><b>Second Term</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.Term#getSecondTerm <em>Second Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second Term</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second Term</em>' reference.
	 * @see #setSecondTerm(Term)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryPredicate_SecondTerm()
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getSecondTerm
	 * @model opposite="secondTerm" required="true"
	 * @generated
	 */
	Term getSecondTerm();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm <em>Second Term</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second Term</em>' reference.
	 * @see #getSecondTerm()
	 * @generated
	 */
	void setSecondTerm(Term value);

	/**
	 * Returns the value of the '<em><b>First Term</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTerm <em>First Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Term</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Term</em>' reference.
	 * @see #setFirstTerm(Term)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryPredicate_FirstTerm()
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getFirstTerm
	 * @model opposite="firstTerm" required="true"
	 * @generated
	 */
	Term getFirstTerm();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm <em>First Term</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Term</em>' reference.
	 * @see #getFirstTerm()
	 * @generated
	 */
	void setFirstTerm(Term value);

} // BinaryPredicate
