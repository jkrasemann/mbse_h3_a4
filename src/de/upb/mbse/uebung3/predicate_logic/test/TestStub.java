package de.upb.mbse.uebung3.predicate_logic.test;

import java.util.Iterator;
import java.util.Map;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.ecore.xmi.impl.XMIResourceFactoryImpl;

import de.upb.mbse.uebung3.predicate_logic.*;
import de.upb.mbse.uebung3.predicate_logic.impl.*;


public class TestStub {
  public static FormulaCollection load(String path) {
    // Initialize the model
	  Predicate_logicPackage.eINSTANCE.eClass();
    
    // Register the XMI resource factory for the .predicate_logic extension
    Resource.Factory.Registry reg = Resource.Factory.Registry.INSTANCE;
    Map<String, Object> m = reg.getExtensionToFactoryMap();
    m.put("predicate_logic", new XMIResourceFactoryImpl());

    // Obtain a new resource set
    ResourceSet resSet = new ResourceSetImpl();

    // Get the resource
    Resource resource = resSet.getResource(URI.createURI(path), true);
    
    // Get the first model element and cast it to the right type, in my
    // example everything is hierarchical included in this first node
    FormulaCollection myObject = (FormulaCollection)resource.getContents().get(0);
    return myObject;
  }
  
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		FormulaCollection collection = load("model/FormulaCollection.predicate_logic");
		
		/* Build the formula "Ex x (istPrim(x) AND kleiner(x,10)) */
		
		/* Obtain a Factory for creating the predicate logic objects*/
		Predicate_logicFactory factory = Predicate_logicFactoryImpl.init();
		
		/* Create a variable x and a constant 10, set its collections and its names */
		Variable x = factory.createVariable();
		x.setCollection(collection);
		x.setName("x");
		
		ConstantSymbol ten = factory.createConstantSymbol();
		ten.setCollection(collection);
		ten.setName("10");
		
		/* Build an Existential quantor and bind it to x */
		Existential ex = factory.createExistential();
		ex.setCollection(collection);
		ex.setHas(x);
		
		/* Create the formulas "istPrim", "kleiner" and a conjunction */
		UnaryPredicate istPrim = factory.createUnaryPredicate();
		istPrim.setCollection(collection);
		istPrim.setFirstTermUnary(x);
		istPrim.setName("istPrim");
		
		BinaryPredicate kleiner = factory.createBinaryPredicate();
		kleiner.setCollection(collection);
		kleiner.setFirstTerm(x);
		kleiner.setSecondTerm(ten);
		kleiner.setName("kleiner");
		
		Conjunction con = factory.createConjunction();
		con.setCollection(collection);
		
		/* Glue everything together */		
		con.setFirstSubFormula(istPrim);
		con.setSecondSubFormula(kleiner);
		ex.setSubFormula(con);
		
		/* Print the formulas using a PrefixVisitor */
		Visitor v = factory.createPrefixVisitor();
		for (Formula f : collection.getFormulas()) {
			f.accept(v);
			System.out.println();
		}
	}


} 