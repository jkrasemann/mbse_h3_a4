/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Predicate</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary <em>First Term Unary</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getUnaryPredicate()
 * @model
 * @generated
 */
public interface UnaryPredicate extends AtomicFormula {
	/**
	 * Returns the value of the '<em><b>First Term Unary</b></em>' reference.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTermUnary <em>First Term Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Term Unary</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Term Unary</em>' reference.
	 * @see #setFirstTermUnary(Term)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getUnaryPredicate_FirstTermUnary()
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getFirstTermUnary
	 * @model opposite="firstTermUnary" required="true"
	 * @generated
	 */
	Term getFirstTermUnary();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary <em>First Term Unary</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Term Unary</em>' reference.
	 * @see #getFirstTermUnary()
	 * @generated
	 */
	void setFirstTermUnary(Term value);

} // UnaryPredicate
