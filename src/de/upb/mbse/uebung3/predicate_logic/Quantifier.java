/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Quantifier</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Quantifier#getHas <em>Has</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getQuantifier()
 * @model abstract="true"
 * @generated
 */
public interface Quantifier extends UnaryOperator {
	/**
	 * Returns the value of the '<em><b>Has</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Has</em>' reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Has</em>' reference.
	 * @see #setHas(Variable)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getQuantifier_Has()
	 * @model required="true"
	 * @generated
	 */
	Variable getHas();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.Quantifier#getHas <em>Has</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Has</em>' reference.
	 * @see #getHas()
	 * @generated
	 */
	void setHas(Variable value);

} // Quantifier
