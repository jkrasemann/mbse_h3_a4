/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Term</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Term#getSecondTerm <em>Second Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTerm <em>First Term</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTermUnary <em>First Term Unary</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Term#getName <em>Name</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Term#getCollection <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm()
 * @model abstract="true"
 * @generated
 */
public interface Term extends EObject {
	/**
	 * Returns the value of the '<em><b>Second Term</b></em>' reference list.
	 * The list contents are of type {@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate}.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm <em>Second Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second Term</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second Term</em>' reference list.
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm_SecondTerm()
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm
	 * @model opposite="secondTerm"
	 * @generated
	 */
	EList<BinaryPredicate> getSecondTerm();

	/**
	 * Returns the value of the '<em><b>First Term</b></em>' reference list.
	 * The list contents are of type {@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate}.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm <em>First Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Term</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Term</em>' reference list.
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm_FirstTerm()
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm
	 * @model opposite="firstTerm"
	 * @generated
	 */
	EList<BinaryPredicate> getFirstTerm();

	/**
	 * Returns the value of the '<em><b>First Term Unary</b></em>' reference list.
	 * The list contents are of type {@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate}.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary <em>First Term Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Term Unary</em>' reference list isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Term Unary</em>' reference list.
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm_FirstTermUnary()
	 * @see de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary
	 * @model opposite="firstTermUnary"
	 * @generated
	 */
	EList<UnaryPredicate> getFirstTermUnary();

	/**
	 * Returns the value of the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Name</em>' attribute isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Name</em>' attribute.
	 * @see #setName(String)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm_Name()
	 * @model
	 * @generated
	 */
	String getName();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.Term#getName <em>Name</em>}' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Name</em>' attribute.
	 * @see #getName()
	 * @generated
	 */
	void setName(String value);

	/**
	 * Returns the value of the '<em><b>Collection</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' container reference.
	 * @see #setCollection(FormulaCollection)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getTerm_Collection()
	 * @see de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getTerms
	 * @model opposite="terms" required="true" transient="false"
	 * @generated
	 */
	FormulaCollection getCollection();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.Term#getCollection <em>Collection</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection</em>' container reference.
	 * @see #getCollection()
	 * @generated
	 */
	void setCollection(FormulaCollection value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String toString();
	
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void accept(Visitor v);

} // Term
