/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Existential</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getExistential()
 * @model
 * @generated
 */
public interface Existential extends Quantifier {
} // Existential
