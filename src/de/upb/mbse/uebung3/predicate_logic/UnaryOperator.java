/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Unary Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.UnaryOperator#getSubFormula <em>Sub Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getUnaryOperator()
 * @model abstract="true"
 * @generated
 */
public interface UnaryOperator extends Formula {
	/**
	 * Returns the value of the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Sub Formula</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Sub Formula</em>' containment reference.
	 * @see #setSubFormula(Formula)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getUnaryOperator_SubFormula()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Formula getSubFormula();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.UnaryOperator#getSubFormula <em>Sub Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Sub Formula</em>' containment reference.
	 * @see #getSubFormula()
	 * @generated
	 */
	void setSubFormula(Formula value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getOperatorSymbol();

} // UnaryOperator
