/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;

import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicFactory
 * @model kind="package"
 * @generated
 */
public interface Predicate_logicPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "predicate_logic";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://predicate_logic/1.0";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "predicate_logic";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Predicate_logicPackage eINSTANCE = de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl.init();

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaImpl <em>Formula</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.FormulaImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getFormula()
	 * @generated
	 */
	int FORMULA = 0;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA__COLLECTION = 0;

	/**
	 * The number of structural features of the '<em>Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_FEATURE_COUNT = 1;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.AtomicFormulaImpl <em>Atomic Formula</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.AtomicFormulaImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getAtomicFormula()
	 * @generated
	 */
	int ATOMIC_FORMULA = 1;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_FORMULA__COLLECTION = FORMULA__COLLECTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_FORMULA__NAME = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Atomic Formula</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int ATOMIC_FORMULA_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryPredicateImpl <em>Unary Predicate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.UnaryPredicateImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUnaryPredicate()
	 * @generated
	 */
	int UNARY_PREDICATE = 2;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_PREDICATE__COLLECTION = ATOMIC_FORMULA__COLLECTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_PREDICATE__NAME = ATOMIC_FORMULA__NAME;

	/**
	 * The feature id for the '<em><b>First Term Unary</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_PREDICATE__FIRST_TERM_UNARY = ATOMIC_FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Predicate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_PREDICATE_FEATURE_COUNT = ATOMIC_FORMULA_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl <em>Binary Predicate</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getBinaryPredicate()
	 * @generated
	 */
	int BINARY_PREDICATE = 3;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PREDICATE__COLLECTION = ATOMIC_FORMULA__COLLECTION;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PREDICATE__NAME = ATOMIC_FORMULA__NAME;

	/**
	 * The feature id for the '<em><b>Second Term</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PREDICATE__SECOND_TERM = ATOMIC_FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>First Term</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PREDICATE__FIRST_TERM = ATOMIC_FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Predicate</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_PREDICATE_FEATURE_COUNT = ATOMIC_FORMULA_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl <em>Term</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.TermImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getTerm()
	 * @generated
	 */
	int TERM = 4;

	/**
	 * The feature id for the '<em><b>Second Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM__SECOND_TERM = 0;

	/**
	 * The feature id for the '<em><b>First Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM__FIRST_TERM = 1;

	/**
	 * The feature id for the '<em><b>First Term Unary</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM__FIRST_TERM_UNARY = 2;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM__NAME = 3;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM__COLLECTION = 4;

	/**
	 * The number of structural features of the '<em>Term</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int TERM_FEATURE_COUNT = 5;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ConstantSymbolImpl <em>Constant Symbol</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.ConstantSymbolImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getConstantSymbol()
	 * @generated
	 */
	int CONSTANT_SYMBOL = 5;

	/**
	 * The feature id for the '<em><b>Second Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL__SECOND_TERM = TERM__SECOND_TERM;

	/**
	 * The feature id for the '<em><b>First Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL__FIRST_TERM = TERM__FIRST_TERM;

	/**
	 * The feature id for the '<em><b>First Term Unary</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL__FIRST_TERM_UNARY = TERM__FIRST_TERM_UNARY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL__NAME = TERM__NAME;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL__COLLECTION = TERM__COLLECTION;

	/**
	 * The number of structural features of the '<em>Constant Symbol</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONSTANT_SYMBOL_FEATURE_COUNT = TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.VariableImpl <em>Variable</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.VariableImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getVariable()
	 * @generated
	 */
	int VARIABLE = 6;

	/**
	 * The feature id for the '<em><b>Second Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__SECOND_TERM = TERM__SECOND_TERM;

	/**
	 * The feature id for the '<em><b>First Term</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__FIRST_TERM = TERM__FIRST_TERM;

	/**
	 * The feature id for the '<em><b>First Term Unary</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__FIRST_TERM_UNARY = TERM__FIRST_TERM_UNARY;

	/**
	 * The feature id for the '<em><b>Name</b></em>' attribute.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__NAME = TERM__NAME;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE__COLLECTION = TERM__COLLECTION;

	/**
	 * The number of structural features of the '<em>Variable</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VARIABLE_FEATURE_COUNT = TERM_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryOperatorImpl <em>Unary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.UnaryOperatorImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUnaryOperator()
	 * @generated
	 */
	int UNARY_OPERATOR = 7;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__COLLECTION = FORMULA__COLLECTION;

	/**
	 * The feature id for the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR__SUB_FORMULA = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Unary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNARY_OPERATOR_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl <em>Binary Operator</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getBinaryOperator()
	 * @generated
	 */
	int BINARY_OPERATOR = 8;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__COLLECTION = FORMULA__COLLECTION;

	/**
	 * The feature id for the '<em><b>First Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__FIRST_SUB_FORMULA = FORMULA_FEATURE_COUNT + 0;

	/**
	 * The feature id for the '<em><b>Second Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR__SECOND_SUB_FORMULA = FORMULA_FEATURE_COUNT + 1;

	/**
	 * The number of structural features of the '<em>Binary Operator</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int BINARY_OPERATOR_FEATURE_COUNT = FORMULA_FEATURE_COUNT + 2;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ConjunctionImpl <em>Conjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.ConjunctionImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getConjunction()
	 * @generated
	 */
	int CONJUNCTION = 9;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__COLLECTION = BINARY_OPERATOR__COLLECTION;

	/**
	 * The feature id for the '<em><b>First Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__FIRST_SUB_FORMULA = BINARY_OPERATOR__FIRST_SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Second Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION__SECOND_SUB_FORMULA = BINARY_OPERATOR__SECOND_SUB_FORMULA;

	/**
	 * The number of structural features of the '<em>Conjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int CONJUNCTION_FEATURE_COUNT = BINARY_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.DisjunctionImpl <em>Disjunction</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.DisjunctionImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getDisjunction()
	 * @generated
	 */
	int DISJUNCTION = 10;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__COLLECTION = BINARY_OPERATOR__COLLECTION;

	/**
	 * The feature id for the '<em><b>First Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__FIRST_SUB_FORMULA = BINARY_OPERATOR__FIRST_SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Second Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION__SECOND_SUB_FORMULA = BINARY_OPERATOR__SECOND_SUB_FORMULA;

	/**
	 * The number of structural features of the '<em>Disjunction</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int DISJUNCTION_FEATURE_COUNT = BINARY_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ImplicationImpl <em>Implication</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.ImplicationImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getImplication()
	 * @generated
	 */
	int IMPLICATION = 11;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION__COLLECTION = BINARY_OPERATOR__COLLECTION;

	/**
	 * The feature id for the '<em><b>First Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION__FIRST_SUB_FORMULA = BINARY_OPERATOR__FIRST_SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Second Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION__SECOND_SUB_FORMULA = BINARY_OPERATOR__SECOND_SUB_FORMULA;

	/**
	 * The number of structural features of the '<em>Implication</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int IMPLICATION_FEATURE_COUNT = BINARY_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.QuantifierImpl <em>Quantifier</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.QuantifierImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getQuantifier()
	 * @generated
	 */
	int QUANTIFIER = 12;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER__COLLECTION = UNARY_OPERATOR__COLLECTION;

	/**
	 * The feature id for the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER__SUB_FORMULA = UNARY_OPERATOR__SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Has</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER__HAS = UNARY_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The number of structural features of the '<em>Quantifier</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int QUANTIFIER_FEATURE_COUNT = UNARY_OPERATOR_FEATURE_COUNT + 1;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ExistentialImpl <em>Existential</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.ExistentialImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getExistential()
	 * @generated
	 */
	int EXISTENTIAL = 13;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENTIAL__COLLECTION = QUANTIFIER__COLLECTION;

	/**
	 * The feature id for the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENTIAL__SUB_FORMULA = QUANTIFIER__SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Has</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENTIAL__HAS = QUANTIFIER__HAS;

	/**
	 * The number of structural features of the '<em>Existential</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int EXISTENTIAL_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UniversalImpl <em>Universal</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.UniversalImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUniversal()
	 * @generated
	 */
	int UNIVERSAL = 14;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL__COLLECTION = QUANTIFIER__COLLECTION;

	/**
	 * The feature id for the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL__SUB_FORMULA = QUANTIFIER__SUB_FORMULA;

	/**
	 * The feature id for the '<em><b>Has</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL__HAS = QUANTIFIER__HAS;

	/**
	 * The number of structural features of the '<em>Universal</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int UNIVERSAL_FEATURE_COUNT = QUANTIFIER_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.NegationImpl <em>Negation</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.NegationImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getNegation()
	 * @generated
	 */
	int NEGATION = 15;

	/**
	 * The feature id for the '<em><b>Collection</b></em>' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION__COLLECTION = UNARY_OPERATOR__COLLECTION;

	/**
	 * The feature id for the '<em><b>Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION__SUB_FORMULA = UNARY_OPERATOR__SUB_FORMULA;

	/**
	 * The number of structural features of the '<em>Negation</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int NEGATION_FEATURE_COUNT = UNARY_OPERATOR_FEATURE_COUNT + 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl <em>Formula Collection</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getFormulaCollection()
	 * @generated
	 */
	int FORMULA_COLLECTION = 16;

	/**
	 * The feature id for the '<em><b>Formulas</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_COLLECTION__FORMULAS = 0;

	/**
	 * The feature id for the '<em><b>Terms</b></em>' containment reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_COLLECTION__TERMS = 1;

	/**
	 * The number of structural features of the '<em>Formula Collection</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int FORMULA_COLLECTION_FEATURE_COUNT = 2;


	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.Visitor <em>Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.Visitor
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getVisitor()
	 * @generated
	 */
	int VISITOR = 17;

	/**
	 * The number of structural features of the '<em>Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int VISITOR_FEATURE_COUNT = 0;

	/**
	 * The meta object id for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.PrefixVisitorImpl <em>Prefix Visitor</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.PrefixVisitorImpl
	 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getPrefixVisitor()
	 * @generated
	 */
	int PREFIX_VISITOR = 18;

	/**
	 * The number of structural features of the '<em>Prefix Visitor</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int PREFIX_VISITOR_FEATURE_COUNT = VISITOR_FEATURE_COUNT + 0;


	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Formula <em>Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Formula
	 * @generated
	 */
	EClass getFormula();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.mbse.uebung3.predicate_logic.Formula#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Collection</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Formula#getCollection()
	 * @see #getFormula()
	 * @generated
	 */
	EReference getFormula_Collection();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.AtomicFormula <em>Atomic Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Atomic Formula</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.AtomicFormula
	 * @generated
	 */
	EClass getAtomicFormula();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.mbse.uebung3.predicate_logic.AtomicFormula#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.AtomicFormula#getName()
	 * @see #getAtomicFormula()
	 * @generated
	 */
	EAttribute getAtomicFormula_Name();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate <em>Unary Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Predicate</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.UnaryPredicate
	 * @generated
	 */
	EClass getUnaryPredicate();

	/**
	 * Returns the meta object for the reference '{@link de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary <em>First Term Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First Term Unary</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.UnaryPredicate#getFirstTermUnary()
	 * @see #getUnaryPredicate()
	 * @generated
	 */
	EReference getUnaryPredicate_FirstTermUnary();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate <em>Binary Predicate</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Predicate</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryPredicate
	 * @generated
	 */
	EClass getBinaryPredicate();

	/**
	 * Returns the meta object for the reference '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm <em>Second Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Second Term</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getSecondTerm()
	 * @see #getBinaryPredicate()
	 * @generated
	 */
	EReference getBinaryPredicate_SecondTerm();

	/**
	 * Returns the meta object for the reference '{@link de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm <em>First Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>First Term</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryPredicate#getFirstTerm()
	 * @see #getBinaryPredicate()
	 * @generated
	 */
	EReference getBinaryPredicate_FirstTerm();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Term <em>Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Term</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term
	 * @generated
	 */
	EClass getTerm();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.mbse.uebung3.predicate_logic.Term#getSecondTerm <em>Second Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Second Term</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getSecondTerm()
	 * @see #getTerm()
	 * @generated
	 */
	EReference getTerm_SecondTerm();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTerm <em>First Term</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>First Term</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getFirstTerm()
	 * @see #getTerm()
	 * @generated
	 */
	EReference getTerm_FirstTerm();

	/**
	 * Returns the meta object for the reference list '{@link de.upb.mbse.uebung3.predicate_logic.Term#getFirstTermUnary <em>First Term Unary</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>First Term Unary</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getFirstTermUnary()
	 * @see #getTerm()
	 * @generated
	 */
	EReference getTerm_FirstTermUnary();

	/**
	 * Returns the meta object for the attribute '{@link de.upb.mbse.uebung3.predicate_logic.Term#getName <em>Name</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the attribute '<em>Name</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getName()
	 * @see #getTerm()
	 * @generated
	 */
	EAttribute getTerm_Name();

	/**
	 * Returns the meta object for the container reference '{@link de.upb.mbse.uebung3.predicate_logic.Term#getCollection <em>Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the container reference '<em>Collection</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Term#getCollection()
	 * @see #getTerm()
	 * @generated
	 */
	EReference getTerm_Collection();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.ConstantSymbol <em>Constant Symbol</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Constant Symbol</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.ConstantSymbol
	 * @generated
	 */
	EClass getConstantSymbol();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Variable <em>Variable</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Variable</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Variable
	 * @generated
	 */
	EClass getVariable();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.UnaryOperator <em>Unary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Unary Operator</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.UnaryOperator
	 * @generated
	 */
	EClass getUnaryOperator();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.mbse.uebung3.predicate_logic.UnaryOperator#getSubFormula <em>Sub Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Sub Formula</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.UnaryOperator#getSubFormula()
	 * @see #getUnaryOperator()
	 * @generated
	 */
	EReference getUnaryOperator_SubFormula();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator <em>Binary Operator</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Binary Operator</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryOperator
	 * @generated
	 */
	EClass getBinaryOperator();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getFirstSubFormula <em>First Sub Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>First Sub Formula</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getFirstSubFormula()
	 * @see #getBinaryOperator()
	 * @generated
	 */
	EReference getBinaryOperator_FirstSubFormula();

	/**
	 * Returns the meta object for the containment reference '{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getSecondSubFormula <em>Second Sub Formula</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference '<em>Second Sub Formula</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getSecondSubFormula()
	 * @see #getBinaryOperator()
	 * @generated
	 */
	EReference getBinaryOperator_SecondSubFormula();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Conjunction <em>Conjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Conjunction</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Conjunction
	 * @generated
	 */
	EClass getConjunction();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Disjunction <em>Disjunction</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Disjunction</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Disjunction
	 * @generated
	 */
	EClass getDisjunction();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Implication <em>Implication</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Implication</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Implication
	 * @generated
	 */
	EClass getImplication();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Quantifier <em>Quantifier</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Quantifier</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Quantifier
	 * @generated
	 */
	EClass getQuantifier();

	/**
	 * Returns the meta object for the reference '{@link de.upb.mbse.uebung3.predicate_logic.Quantifier#getHas <em>Has</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Has</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Quantifier#getHas()
	 * @see #getQuantifier()
	 * @generated
	 */
	EReference getQuantifier_Has();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Existential <em>Existential</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Existential</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Existential
	 * @generated
	 */
	EClass getExistential();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Universal <em>Universal</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Universal</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Universal
	 * @generated
	 */
	EClass getUniversal();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Negation <em>Negation</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Negation</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Negation
	 * @generated
	 */
	EClass getNegation();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection <em>Formula Collection</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Formula Collection</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.FormulaCollection
	 * @generated
	 */
	EClass getFormulaCollection();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getFormulas <em>Formulas</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Formulas</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getFormulas()
	 * @see #getFormulaCollection()
	 * @generated
	 */
	EReference getFormulaCollection_Formulas();

	/**
	 * Returns the meta object for the containment reference list '{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getTerms <em>Terms</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the containment reference list '<em>Terms</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getTerms()
	 * @see #getFormulaCollection()
	 * @generated
	 */
	EReference getFormulaCollection_Terms();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.Visitor <em>Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Visitor</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.Visitor
	 * @generated
	 */
	EClass getVisitor();

	/**
	 * Returns the meta object for class '{@link de.upb.mbse.uebung3.predicate_logic.PrefixVisitor <em>Prefix Visitor</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Prefix Visitor</em>'.
	 * @see de.upb.mbse.uebung3.predicate_logic.PrefixVisitor
	 * @generated
	 */
	EClass getPrefixVisitor();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Predicate_logicFactory getPredicate_logicFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaImpl <em>Formula</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.FormulaImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getFormula()
		 * @generated
		 */
		EClass FORMULA = eINSTANCE.getFormula();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA__COLLECTION = eINSTANCE.getFormula_Collection();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.AtomicFormulaImpl <em>Atomic Formula</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.AtomicFormulaImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getAtomicFormula()
		 * @generated
		 */
		EClass ATOMIC_FORMULA = eINSTANCE.getAtomicFormula();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute ATOMIC_FORMULA__NAME = eINSTANCE.getAtomicFormula_Name();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryPredicateImpl <em>Unary Predicate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.UnaryPredicateImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUnaryPredicate()
		 * @generated
		 */
		EClass UNARY_PREDICATE = eINSTANCE.getUnaryPredicate();

		/**
		 * The meta object literal for the '<em><b>First Term Unary</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_PREDICATE__FIRST_TERM_UNARY = eINSTANCE.getUnaryPredicate_FirstTermUnary();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl <em>Binary Predicate</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.BinaryPredicateImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getBinaryPredicate()
		 * @generated
		 */
		EClass BINARY_PREDICATE = eINSTANCE.getBinaryPredicate();

		/**
		 * The meta object literal for the '<em><b>Second Term</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_PREDICATE__SECOND_TERM = eINSTANCE.getBinaryPredicate_SecondTerm();

		/**
		 * The meta object literal for the '<em><b>First Term</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_PREDICATE__FIRST_TERM = eINSTANCE.getBinaryPredicate_FirstTerm();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.TermImpl <em>Term</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.TermImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getTerm()
		 * @generated
		 */
		EClass TERM = eINSTANCE.getTerm();

		/**
		 * The meta object literal for the '<em><b>Second Term</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERM__SECOND_TERM = eINSTANCE.getTerm_SecondTerm();

		/**
		 * The meta object literal for the '<em><b>First Term</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERM__FIRST_TERM = eINSTANCE.getTerm_FirstTerm();

		/**
		 * The meta object literal for the '<em><b>First Term Unary</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERM__FIRST_TERM_UNARY = eINSTANCE.getTerm_FirstTermUnary();

		/**
		 * The meta object literal for the '<em><b>Name</b></em>' attribute feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EAttribute TERM__NAME = eINSTANCE.getTerm_Name();

		/**
		 * The meta object literal for the '<em><b>Collection</b></em>' container reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference TERM__COLLECTION = eINSTANCE.getTerm_Collection();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ConstantSymbolImpl <em>Constant Symbol</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.ConstantSymbolImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getConstantSymbol()
		 * @generated
		 */
		EClass CONSTANT_SYMBOL = eINSTANCE.getConstantSymbol();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.VariableImpl <em>Variable</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.VariableImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getVariable()
		 * @generated
		 */
		EClass VARIABLE = eINSTANCE.getVariable();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UnaryOperatorImpl <em>Unary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.UnaryOperatorImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUnaryOperator()
		 * @generated
		 */
		EClass UNARY_OPERATOR = eINSTANCE.getUnaryOperator();

		/**
		 * The meta object literal for the '<em><b>Sub Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference UNARY_OPERATOR__SUB_FORMULA = eINSTANCE.getUnaryOperator_SubFormula();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl <em>Binary Operator</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.BinaryOperatorImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getBinaryOperator()
		 * @generated
		 */
		EClass BINARY_OPERATOR = eINSTANCE.getBinaryOperator();

		/**
		 * The meta object literal for the '<em><b>First Sub Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_OPERATOR__FIRST_SUB_FORMULA = eINSTANCE.getBinaryOperator_FirstSubFormula();

		/**
		 * The meta object literal for the '<em><b>Second Sub Formula</b></em>' containment reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference BINARY_OPERATOR__SECOND_SUB_FORMULA = eINSTANCE.getBinaryOperator_SecondSubFormula();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ConjunctionImpl <em>Conjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.ConjunctionImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getConjunction()
		 * @generated
		 */
		EClass CONJUNCTION = eINSTANCE.getConjunction();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.DisjunctionImpl <em>Disjunction</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.DisjunctionImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getDisjunction()
		 * @generated
		 */
		EClass DISJUNCTION = eINSTANCE.getDisjunction();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ImplicationImpl <em>Implication</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.ImplicationImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getImplication()
		 * @generated
		 */
		EClass IMPLICATION = eINSTANCE.getImplication();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.QuantifierImpl <em>Quantifier</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.QuantifierImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getQuantifier()
		 * @generated
		 */
		EClass QUANTIFIER = eINSTANCE.getQuantifier();

		/**
		 * The meta object literal for the '<em><b>Has</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference QUANTIFIER__HAS = eINSTANCE.getQuantifier_Has();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.ExistentialImpl <em>Existential</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.ExistentialImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getExistential()
		 * @generated
		 */
		EClass EXISTENTIAL = eINSTANCE.getExistential();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.UniversalImpl <em>Universal</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.UniversalImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getUniversal()
		 * @generated
		 */
		EClass UNIVERSAL = eINSTANCE.getUniversal();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.NegationImpl <em>Negation</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.NegationImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getNegation()
		 * @generated
		 */
		EClass NEGATION = eINSTANCE.getNegation();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl <em>Formula Collection</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.FormulaCollectionImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getFormulaCollection()
		 * @generated
		 */
		EClass FORMULA_COLLECTION = eINSTANCE.getFormulaCollection();

		/**
		 * The meta object literal for the '<em><b>Formulas</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_COLLECTION__FORMULAS = eINSTANCE.getFormulaCollection_Formulas();

		/**
		 * The meta object literal for the '<em><b>Terms</b></em>' containment reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference FORMULA_COLLECTION__TERMS = eINSTANCE.getFormulaCollection_Terms();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.Visitor <em>Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.Visitor
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getVisitor()
		 * @generated
		 */
		EClass VISITOR = eINSTANCE.getVisitor();

		/**
		 * The meta object literal for the '{@link de.upb.mbse.uebung3.predicate_logic.impl.PrefixVisitorImpl <em>Prefix Visitor</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.PrefixVisitorImpl
		 * @see de.upb.mbse.uebung3.predicate_logic.impl.Predicate_logicPackageImpl#getPrefixVisitor()
		 * @generated
		 */
		EClass PREFIX_VISITOR = eINSTANCE.getPrefixVisitor();

	}

} //Predicate_logicPackage
