/**
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Prefix Visitor</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getPrefixVisitor()
 * @model
 * @generated
 */
public interface PrefixVisitor extends Visitor {
} // PrefixVisitor
