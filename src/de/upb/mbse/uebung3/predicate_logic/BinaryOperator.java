/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Binary Operator</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getFirstSubFormula <em>First Sub Formula</em>}</li>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getSecondSubFormula <em>Second Sub Formula</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryOperator()
 * @model abstract="true"
 * @generated
 */
public interface BinaryOperator extends Formula {
	/**
	 * Returns the value of the '<em><b>First Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>First Sub Formula</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>First Sub Formula</em>' containment reference.
	 * @see #setFirstSubFormula(Formula)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryOperator_FirstSubFormula()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Formula getFirstSubFormula();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getFirstSubFormula <em>First Sub Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>First Sub Formula</em>' containment reference.
	 * @see #getFirstSubFormula()
	 * @generated
	 */
	void setFirstSubFormula(Formula value);

	/**
	 * Returns the value of the '<em><b>Second Sub Formula</b></em>' containment reference.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Second Sub Formula</em>' containment reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Second Sub Formula</em>' containment reference.
	 * @see #setSecondSubFormula(Formula)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getBinaryOperator_SecondSubFormula()
	 * @model containment="true" required="true"
	 * @generated
	 */
	Formula getSecondSubFormula();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.BinaryOperator#getSecondSubFormula <em>Second Sub Formula</em>}' containment reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Second Sub Formula</em>' containment reference.
	 * @see #getSecondSubFormula()
	 * @generated
	 */
	void setSecondSubFormula(Formula value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model kind="operation"
	 * @generated
	 */
	String getOperatorSymbol();

} // BinaryOperator
