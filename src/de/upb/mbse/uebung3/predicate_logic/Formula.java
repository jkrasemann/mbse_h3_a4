/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;

import org.eclipse.emf.ecore.EObject;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Formula</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * <ul>
 *   <li>{@link de.upb.mbse.uebung3.predicate_logic.Formula#getCollection <em>Collection</em>}</li>
 * </ul>
 * </p>
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getFormula()
 * @model abstract="true"
 * @generated
 */
public interface Formula extends EObject {
	/**
	 * Returns the value of the '<em><b>Collection</b></em>' container reference.
	 * It is bidirectional and its opposite is '{@link de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getFormulas <em>Formulas</em>}'.
	 * <!-- begin-user-doc -->
	 * <p>
	 * If the meaning of the '<em>Collection</em>' container reference isn't clear,
	 * there really should be more of a description here...
	 * </p>
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Collection</em>' container reference.
	 * @see #setCollection(FormulaCollection)
	 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getFormula_Collection()
	 * @see de.upb.mbse.uebung3.predicate_logic.FormulaCollection#getFormulas
	 * @model opposite="formulas" transient="false"
	 * @generated
	 */
	FormulaCollection getCollection();

	/**
	 * Sets the value of the '{@link de.upb.mbse.uebung3.predicate_logic.Formula#getCollection <em>Collection</em>}' container reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Collection</em>' container reference.
	 * @see #getCollection()
	 * @generated
	 */
	void setCollection(FormulaCollection value);

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	String toString();

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @model
	 * @generated
	 */
	void accept(Visitor v);

} // Formula
