/**
 * <copyright>
 * </copyright>
 *
 * $Id$
 */
package de.upb.mbse.uebung3.predicate_logic;


/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Conjunction</b></em>'.
 * <!-- end-user-doc -->
 *
 *
 * @see de.upb.mbse.uebung3.predicate_logic.Predicate_logicPackage#getConjunction()
 * @model
 * @generated
 */
public interface Conjunction extends BinaryOperator {
} // Conjunction
